FROM debian:bookworm
MAINTAINER Accomodata <support@accomodata.be>


RUN set -e \
    # Update APT cache & install upgrades
    && apt-get update \
    && apt-get upgrade -y \
    # Install packages
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        bash \
        curl \
        jq \
        python3-pip \
    # Do cleanup
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && pip3 install --break-system-packages awxkit \
    # Done
    && echo 'Done'

